## Docker Image with base Xenial Install
This image was build from [ubuntu:xenial](https://hub.docker.com/_/ubuntu/).

The university repos have been setup here, so that users can `apt install` in later images without needing to add the repo's.  

## Usage
### Building your own image
Please use our repo at the top of your Dockerfile. e.g.
```
FROM git.cs.sun.ac.za:4567/computer-science/docker-prebuild/sun-xenial-base
```
### Running this image
You can run this image directly:
```
docker run -it git.cs.sun.ac.za:4567/computer-science/docker-prebuild/sun-xenial-base bash
```