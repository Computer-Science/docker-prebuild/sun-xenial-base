FROM ubuntu:xenial

RUN echo "\
deb http://ftp.sun.ac.za/ubuntu xenial main universe multiverse restricted \n \
deb http://ftp.sun.ac.za/ubuntu xenial-security main universe multiverse restricted \n \
deb http://ftp.sun.ac.za/ubuntu xenial-updates main universe multiverse restricted \n \
deb http://ftp.sun.ac.za/ubuntu xenial-backports main universe multiverse restricted \n \
deb http://ubuntu.mirror.ac.za/ubuntu xenial main universe multiverse restricted \n \
deb http://ubuntu.mirror.ac.za/ubuntu xenial-security main universe multiverse restricted \n \
deb http://ubuntu.mirror.ac.za/ubuntu xenial-updates main universe multiverse restricted \n \
deb http://ubuntu.mirror.ac.za/ubuntu xenial-backports main universe multiverse restricted " > /etc/apt/sources.list

